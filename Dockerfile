# Create a container based on Amazon Linux 2.0 with Nginx 1.22 and PHP 8.0.
# Build command example: docker build -t "spycetek/aep:a2.0-1.22-8.0-1" .

FROM amazonlinux:2.0.20230320.0

# Prepare for package installation
RUN yum update -y

# Add basic commands available on Amazon Linux 2 EC2
# procps: basic commands like ps
RUN yum install -y \
    file \
    git \
    procps \
    unzip \
    sudo \
    vim \
    wget

# Install Nginx & PHP 8.0
RUN amazon-linux-extras install -y \
    nginx1 \
    php8.0

# php-pdo, php-fpm, php-mysqlnd, php-cli

# php-*: Install packages for PHP 8.0
# mariadb: MySQL client. mariadb is available by default, but mysql.
RUN yum install -y \
    php-gd \
    php-intl \
    php-json \
    php-mbstring \
    php-openssl \
    php-xml \
    mariadb

### PHP FPM Configuration for Nginx
# Create PHP FPM execution user 'webapp' along with 'webapp' group, which is used on AWS EB platform.
RUN useradd webapp
# Change PHP FPM execution user from apache to 'webapp'
RUN if grep -iq "^user *=" /etc/php-fpm.d/www.conf; then \
        sed -i -E 's/^(user *=.*)$/;\1\nuser = webapp/' /etc/php-fpm.d/www.conf; \
    else \
        echo 'user = webapp' >> /etc/php-fpm.d/www.conf; \
    fi
# Change PHP FPM execution group from apache to 'webapp'
RUN if grep -iq "^group *=" /etc/php-fpm.d/www.conf; then \
        sed -i -E 's/^(group *=.*)$/;\1\ngroup = webapp/' /etc/php-fpm.d/www.conf; \
    else \
        echo 'group = webapp' >> /etc/php-fpm.d/www.conf; \
    fi
# Fix to make PHP FPM work with Nginx
# listen.owner & listen.group should be set to web server user 'nginx'
RUN if grep -iq "^listen.acl_users *=" /etc/php-fpm.d/www.conf; then \
        sed -i -E 's/^(listen.acl_users *=.*)$/;\1/' /etc/php-fpm.d/www.conf; \
        sed -i -E 's/^;* *(listen.owner *=.*)$/;\1\nlisten.owner = nginx/' /etc/php-fpm.d/www.conf; \
        sed -i -E 's/^;* *(listen.group *=.*)$/;\1\nlisten.group = nginx/' /etc/php-fpm.d/www.conf; \
    fi

# Enable auto start
RUN systemctl enable nginx && \
    systemctl enable php-fpm

# Install AWS CLI tools
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

# Install composer
RUN curl -s https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer

# Install npm for webpack, SCSS, and TypeScript
RUN curl -sL https://rpm.nodesource.com/setup_16.x | bash - && \
    yum install -y nodejs

# Configure root bashrc
COPY files/root_bashrc /root/.bashrc

VOLUME [ "/sys/fs/cgroup" ]

## Install Pear for ast and xdebug
#RUN amazon-linux-extras install epel && \
#    yum install -y php-devel php-pear
#
## Install ast for phan
#RUN pecl install ast && \
#    echo "extension=ast.so" > /etc/php.d/40-spycetek.ini
#
## Install and Configure xdebug
## php-xdebug: for debugging and PhpUnit coverage
#RUN pecl install xdebug
#RUN echo "zend_extension=/usr/lib64/php/modules/xdebug.so" > /etc/php.d/40-xdebug.ini && \
#    echo "xdebug.remote_enable = 1" >> /etc/php.d/40-xdebug.ini && \
#    echo "xdebug.remote_host = host.docker.internal" >> /etc/php.d/40-xdebug.ini && \
#    echo "xdebug.remote_port = 9001" >> /etc/php.d/40-xdebug.ini
#
## Disable xdebug by default because it slows down processing php a lot.
## To enable, rename the file to `40-xdebug.ini`, then restart fpm.
#RUN mv /etc/php.d/40-xdebug.ini /etc/php.d/40-xdebug.ini.disabled

## Allow nginx user to execute supervisorctl commands
#RUN yum install -y sudo
#COPY files/sudoers.config /etc/sudoers.d/supervisor
#
##COPY start_nginx_php-fpm.sh /root/start_nginx_php-fpm.sh
##RUN chmod +x /root/start_nginx_php-fpm.sh
