# AEP Docker Container
Docker container of AEP: Amazon Linux 2 + Nginx + PHP system.

Docker Hub: [spycetek/aep](https://cloud.docker.com/u/spycetek/repository/docker/spycetek/aep)

## Usage
Add entry point `/root/start_nginx_php-fpm.sh`.

Sample docker-compose.yml
```
version: '3'
services:
  web-server:
    image: spycetek/aep:a2.0-1.20-8.0-1
    privileged: true
    command: /sbin/init
    container_name: web-server
    restart: unless-stopped
    ports:
      - 50080:80
    networks:
      - mynetwork
    volumes:
      - /path/in/your/local/machine:/var/www/html:cached

# Network is required to enable container access another container.
networks:
  mynetwork:
    name: local-net
    external: true
```


## Contributing
### Tags
Please follow the tag name convention below.

a{Amazon Linux version}-{Nginx version}-{PHP version}-{Image version}

* `a` stands for Amazon Linux.
* {Image version} is the version among the same combination of versions of Amazon Linux, Nginx, and PHP.  
  So, {Image version} should be reset to `1` when any of them updated.

Example: a2.0-1.20-8.0-1

### How to build
#### Build on Docker Hub
You don't have to build since Docker Hub ([spycetek/aep](https://cloud.docker.com/u/spycetek/repository/docker/spycetek/aep)) is configured to build a new tag automatically.
You just need to tag your commit to this source file repository and push it.
Don't forget to push tags as well (by `git push --tags`).

#### Building Locally
If you want to build the image locally, execute the command below in the directory where Dockerfile resides.

Note that there is "." (dot) at the end indicating the current directory.  
This process takes quite some time. It is recommended to run this under fast internet connection.
```shell
docker build -t "spycetek/aep:a2.0-1.20-8.0-1" .
```

#### Pushing to Docker Hub
You won't need to, but if you ever want to push an image to Docker Hub, this is how.
The DockerHub for this container is configured to build when new Dockerfile and tag is uploaded to the repository.

Commit the changes and tag the commit with `git`.
```
git tag "a2.0-1.20-8.0-1"
git push
git push --tags
```
DockerHub hooks this push to repository and start building the image.
